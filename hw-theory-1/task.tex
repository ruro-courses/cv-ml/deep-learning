\documentclass{article}

\usepackage[active,tightpage]{preview}
\usepackage{geometry}
\usepackage[utf8]{inputenc}
\usepackage[T2A]{fontenc}
\usepackage[english,russian]{babel}
\usepackage{tikz}
\usetikzlibrary{graphs, calc, shapes.geometric, positioning, math}
\usetikzlibrary{fit}
\usetikzlibrary{backgrounds}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{enumitem}
\usepackage{listings}
\usepackage{mathdots}
\usepackage{mathtools}
\usepackage{nccmath}
\usepackage{tasks}
\usepackage{textcomp}

\lstset{
    showstringspaces=false,
    columns=fullflexible,
    xleftmargin=0.5cm,
    tabsize=4,
    escapeinside={<@}{@>},
    basicstyle=\ttfamily,
    commentstyle=\bf,
    keywordstyle=\color{blue},
    stringstyle=\color{purple},
}

\DeclareFontFamily{OT1}{cmrx}{}
\DeclareFontShape{OT1}{cmrx}{m}{n}{<->cmr10}{}

\renewcommand{\PreviewBorder}{12pt}
\setenumerate[1]{label={(\alph*)}}

\newcommand{\makeline}{\noindent\makebox[\linewidth]{\rule{\paperwidth}{0.4pt}}}
\newcommand{\sprod}[2]{\left<#1,\ #2\right>}
\newcommand{\norm}[1]{\left\|#1\right\|}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\pad}{pad}
\DeclareMathOperator{\flip}{flip}
\DeclareMathOperator{\height}{height}
\DeclareMathOperator{\width}{width}

\theoremstyle{definition}
\newtheorem{problem}{Problem}[part]
\renewcommand{\theproblem}{\arabic{problem}}
\newtheorem*{solution}{Solution}
\newtheorem{csolution}{Solution}[section]
\numberwithin{csolution}{problem}
\renewcommand{\thecsolution}{(\alph{csolution})}

\begin{document}
    \begin{preview}
        \begin{center}
        {\Huge Deep Learning}
            \\[\baselineskip]

            \textbf{\Large Homework 1:}

            \textbf{Matrix Differentiation}
            \\[0.5\baselineskip]

            \textit{Стоцкий Андрей Владимирович, 622 гр.}
        \end{center}

        \makeline\\[-0.75\baselineskip]\makeline
        \begin{problem} % (1)
            Find the gradient $\nabla_{X}f$ for function $f\left(X\right) = \det\left(X^{-1} + A\right)$, where $X \in \mathbb{R}^{n{\times}n}$ is some non-degenerate matrix, and $A \in \mathbb{R}^{n{\times}n}$ is an arbitrary matrix such that $X^{-1} + A$ is non-degenerate.
        \end{problem}
        \makeline
        \begin{solution}
            ~

            \begin{gather*}
                df(X) = d\left(det\left(X^{-1} + A\right)\right) = \\
                = \det\left(X^{-1} + A\right) \sprod{{\left(X^{-1} + A\right)}^{-T}}{d\left(X^{-1} + A\right)} = \\
                = -\det\left(X^{-1} + A\right) \sprod{{\left(X^{-1} + A\right)}^{-T}}{X^{-1} (dX) X^{-1}} = \\
                = \sprod{-\det\left(X^{-1} + A\right) X^{-T} {\left(X^{-1} + A\right)}^{-T} X^{-T}}{dX}
            \end{gather*}
            \begin{gather*}
                \nabla_X f(X) = -\det\left(X^{-1} + A\right) X^{-T} {\left(X^{-1} + A\right)}^{-T} X^{-T} = \\
                = -\det\left(X^{-1} + A\right) {\left(X \left(X^{-1} + A\right) X\right)}^{-T} = \\
                = -\det\left(X^{-1} + A\right) {\left(X + XAX\right)}^{-T}
            \end{gather*}
        \end{solution}

        \makeline\\[-0.75\baselineskip]\makeline
        \begin{problem} % (2)
            For each of the following functions $f$ find gradient $\nabla f$ and Hessian $\nabla^{2} f$:
            \begin{enumerate}
                \item $f: \mathbb{R}_{++} \rightarrow \mathbb{R}$, $f(t)=\left\|\left(A+t I_{n}\right)^{-1} b\right\|$, where $A \in \mathbb{S}_{+}^{n}, b \in \mathbb{R}^{n}$.
                \item $f: \mathbb{R}^{n} \rightarrow \mathbb{R}$, $f(x)=\frac{1}{2}\left\|x x^{T}-A\right\|_{F}^{2}$, where $A \in \mathbb{S}^{n}$.
            \end{enumerate}
        \end{problem}
        \makeline
        \begin{csolution} % (2.a)
            ~

            The function $f$ is a scalar function.
            Its gradient is equivalent to its first-order derivative and its Hessian is equivalent to its second-order derivative.
            \\[0.5\baselineskip]
            Let $g(t) = \sprod{\left(A + tI_n\right)^{-1}b}{\left(A + tI_n\right)^{-1}b}$.

            \begin{gather*}
                dg (t) = d\sprod{\left(A + tI_n\right)^{-1}b}{\left(A + tI_n\right)^{-1}b} = \\
                = 2\sprod{d\left(\left(A + tI_n\right)^{-1}b\right)}{\left(A + tI_n\right)^{-1}b} = \\
                = 2\sprod{-\left(A + tI_n\right)^{-1}d\left(A + tI_n\right)\left(A + tI_n\right)^{-1}b}{\left(A + tI_n\right)^{-1}b} = \\
                = -2\sprod{\left(A + tI_n\right)^{-2}b}{\left(A + tI_n\right)^{-1}b}dt
            \end{gather*}

            Using the above formula, let us find the first derivative of $f(t)$

            \begin{gather*}
                df (t) = d\norm{\left(A + tI_n\right)^{-1}b} = \\
                = d\left(\sprod{\left(A + tI_n\right)^{-1}b}{\left(A + tI_n\right)^{-1}b}^{1/2}\right) = \\
                = d\left(g(t)^{1/2}\right) = \frac{dg(t)}{2\sqrt{g(t)}} = \\
                = -\frac{\sprod{\left(A + tI_n\right)^{-2}b}{\left(A + tI_n\right)^{-1}b}}{\norm{\left(A + tI_n\right)^{-1}b}} dt
            \end{gather*}

            \begin{gather*}
                f'(t) = -\frac{\sprod{\left(A + tI_n\right)^{-2}b}{\left(A + tI_n\right)^{-1}b}}{\norm{\left(A + tI_n\right)^{-1}b}} = \\
                = -\frac{\sprod{\left(A + tI_n\right)^{-2}b}{\left(A + tI_n\right)^{-1}b}}{f(t)}
            \end{gather*}

            Let $p(t) = \sprod{\left(A + tI_n\right)^{-2}b}{\left(A + tI_n\right)^{-1}b}$.

            \begin{gather*}
                dp(t) = d\sprod{\left(A + tI_n\right)^{-2}b}{\left(A + tI_n\right)^{-1}b} = \\
                = \sprod{d\left(\left(A + tI_n\right)^{-2}b\right)}{\left(A + tI_n\right)^{-1}b}
                +\sprod{\left(A + tI_n\right)^{-2}b}{d\left(\left(A + tI_n\right)^{-1}b\right)} = \\
                = -2\sprod{\left(A + tI_n\right)^{-3}b}{\left(A + tI_n\right)^{-1}b} dt
                -\sprod{\left(A + tI_n\right)^{-2}b}{\left(A + tI_n\right)^{-2}b} dt
            \end{gather*}

            Using the above formula, let us find the second derivative of $f(t)$

            \begin{gather*}
                d f'\left(t\right) = d\left(-\frac{p(t)}{f(t)}\right) = \\
                = \frac{p(t) df(t) - f(t) dp(t)}{f(t)^2} = \frac{p(t) df(t)}{f(t)^2} - \frac{dp(t)}{f(t)} = \\
                = -\frac{\sprod{\left(A + tI_n\right)^{-2}b}{\left(A + tI_n\right)^{-1}b}^2}{f(t)^3} dt \\
                +\frac{2\sprod{\left(A + tI_n\right)^{-3}b}{\left(A + tI_n\right)^{-1}b}
                +\sprod{\left(A + tI_n\right)^{-2}b}{\left(A + tI_n\right)^{-2}b}}{f(t)} dt
            \end{gather*}

            \begin{gather*}
                f''(t) = -\frac{\sprod{\left(A + tI_n\right)^{-2}b}{\left(A + tI_n\right)^{-1}b}^2}{f(t)^3} \\
                +\frac{2\sprod{\left(A + tI_n\right)^{-3}b}{\left(A + tI_n\right)^{-1}b}
                +\sprod{\left(A + tI_n\right)^{-2}b}{\left(A + tI_n\right)^{-2}b}}{f(t)}
            \end{gather*}
        \end{csolution}
        \makeline
        \begin{csolution} % (2.b)
            ~

            \begin{gather*}
                f(x) = \frac{1}{2}\norm{xx^T - A}^2_F = \\
                = \frac{1}{2}\sprod{xx^T - A}{xx^T - A} = \\
                = \frac{1}{2}\sprod{xx^T}{xx^T} - \frac{1}{2}\sprod{A}{xx^T} - \frac{1}{2}\sprod{xx^T}{A} + \sprod{A}{A} = \\
                = \frac{1}{2}\sprod{xx^T}{xx^T} - \sprod{xx^T}{A} + \frac{1}{2}\sprod{A}{A} = \\
                = \frac{1}{2}\tr\left(\left(xx^T\right)^T xx^T\right) - \sprod{xx^T}{A}  + \frac{1}{2}\sprod{A}{A} = \\
                = \frac{1}{2}\tr\left(xx^T xx^T\right) - \sprod{xx^T}{A} + \frac{1}{2}\sprod{A}{A} = \\
                = \frac{\norm{x}^4}{2} - \sprod{xx^T}{A} + \frac{\sprod{A}{A}}{2}
            \end{gather*}

            Using the above formula, let us find the gradient of $f(x)$

            \begin{gather*}
                df(x) = d\left(\frac{\norm{x}^4}{2} - \sprod{xx^T}{A}  + \frac{\sprod{A}{A}}{2}\right) = \\
                = \frac{d\left(\sprod{x}{x}^2\right)}{2} - \sprod{d\left(xx^T\right)}{A} = \\
                = \sprod{x}{x}d\sprod{x}{x} - \sprod{dxx^T + xdx^T}{A} = \\
                = 2\sprod{x}{x}\sprod{x}{dx} - \sprod{dxx^T + xdx^T}{A} = \\
                = \sprod{2x\norm{x}^2}{dx} - \sprod{dxx^T}{A} - \sprod{xdx^T}{A} = \\
                = \sprod{2x\norm{x}^2}{dx} - \sprod{A x}{dx} - \sprod{A^T x}{dx} = \\
                = \sprod{2x\norm{x}^2 - (A+A^T)x}{dx} = \\
                = \sprod{2x\norm{x}^2 - 2Ax}{dx}
            \end{gather*}

            \[
                \nabla f(x) = 2x\norm{x}^2 - 2Ax
            \]

            Using the above formula, let us find the Hessian of $f(x)$

            \begin{gather*}
                d^2 f(x) = d\left(\sprod{2x\norm{x}^2 - 2Ax}{dx_1}\right) = \\
                = \sprod{2d\left(x\norm{x}^2\right) - 2d\left(Ax\right)}{dx_1} = \\
                = \sprod{2\norm{x}^2 dx_2 + 2xd\sprod{x}{x} - 2Adx_2}{dx_1} = \\
                = \sprod{2\norm{x}^2 dx_2 + 4x\sprod{x}{dx_2} - 2Adx_2}{dx_1} = \\
                = \sprod{2\norm{x}^2 dx_2 + 4xx^T dx_2 - 2Adx_2}{dx_1} = \\
                = \sprod{2\left(\norm{x}^2 I_n + 2xx^T - A\right) dx_2}{dx_1} = \\
                = \sprod{2\left(\norm{x}^2 I_n + 2xx^T - A\right) dx_1}{dx_2}
            \end{gather*}

            \[
                \nabla^2 f(x) = 2\left(\norm{x}^2 I_n + 2xx^T - A\right)
            \]
        \end{csolution}

        \makeline\\[-0.75\baselineskip]\makeline
        \begin{problem} % (3)
            For each of the following functions $f$ find all stationary points and indicate possible parameter values when they exist:
            \begin{enumerate}
                \item $f: E \rightarrow \mathbb{R}$, $f(x)=\langle a, x\rangle-\ln (1-\langle b, x\rangle)$, where $a, b \in \mathbb{R}^{n}, a, b \neq 0, E=\left\{x \in \mathbb{R}^{n} \mid\langle b, x\rangle<1\right\}$.
                \item $f: \mathbb{R}^{n} \rightarrow \mathbb{R}$, $f(x)=\langle c, x\rangle \exp (-\langle A x, x\rangle)$, where $c \in \mathbb{R}^{n}, c \neq 0, A \in \mathbb{S}_{++}^{n}$.
                \item $f: \mathbb{S}_{++}^{n} \rightarrow \mathbb{R}$, $f(X)=\left\langle X^{-1}, I_{n}\right\rangle-\langle A, X\rangle$, where $A \in \mathbb{S}^{n}$.
            \end{enumerate}
        \end{problem}
        \makeline
        \begin{csolution} % (3.a)
            ~

            Find the derivative:
            \begin{gather*}
                d f (x) = \sprod{a}{dx} - d\ln\left(1 - \sprod{b}{x}\right) = \\
                = \sprod{a}{dx} - \frac{1}{1 - \sprod{b}{x}}\sprod{b}{dx} = \\
                = \sprod{a - \frac{b}{1 - \sprod{b}{x}}}{dx}
            \end{gather*}

            Using the derivative, find the stationary points:
            \begin{gather*}
                f'(x) = a - \frac{b}{1 - \sprod{b}{x}} = 0\\
                a \left(1 - \sprod{b}{x}\right) = b
            \end{gather*}

            Note, that $1 - \sprod{b}{x}$ is a scalar and $a,\ b \neq 0$.
            Therefore, the equation holds if and only if $a = kb$.
            \begin{gather*}
                kb \left(1 - \sprod{b}{x}\right) = b\\
                \sprod{b}{x} = \frac{k-1}{k}
            \end{gather*}

            Given that $\sprod{b}{x} < 1$, we can conclude that $\frac{k-1}{k} < 1$ and $k > 0$.

            Conclusion: stationary points exist only for $a = kb,\ k > 0$ and are located in the hyperplane defined by the equation $\sprod{b}{x} = \frac{k-1}{k}$.
        \end{csolution}
        \makeline
        \begin{csolution} % (3.b)
            ~

            Find the gradient:
            \begin{gather*}
                d f(x) = d\left(\sprod{c}{x}\exp\left(-\sprod{Ax}{x}\right)\right) = \\
                = \exp\left(-\sprod{Ax}{x}\right)\sprod{c}{dx} - 2\sprod{c}{x}\exp\left(-\sprod{Ax}{x}\right)\sprod{Ax}{dx} = \\
                = \sprod{\exp\left(-\sprod{Ax}{x}\right)c - 2\sprod{c}{x}\exp\left(-\sprod{Ax}{x}\right)Ax}{dx} = \\
                = \sprod{\left(c - 2\sprod{c}{x}Ax\right)\exp\left(-\sprod{Ax}{x}\right)}{dx}
            \end{gather*}

            Using the gradient, find the stationary points:
            \begin{gather*}
                \nabla f(x) = \left(c - 2\sprod{c}{x}Ax\right)\exp\left(-\sprod{Ax}{x}\right) = 0\\
                c - 2\sprod{c}{x}Ax = 0\\
                2\sprod{c}{x}Ax = c\\
                2\sprod{c}{x}x = A^{-1}c\\
                2x\sprod{c}{x} = A^{-1}c
            \end{gather*}

            Multiply each side by $c^T$ on the left:
            \begin{gather*}
                2c^Tx\sprod{c}{x} = c^TA^{-1}c\\
                \sprod{c}{x}\sprod{c}{x} = \frac{1}{2}c^TA^{-1}c\\
                \sprod{c}{x} = \sqrt{\frac{1}{2}c^TA^{-1}c\,}\\
            \end{gather*}

            Substitute this back into the previous equality:
            \begin{gather*}
                2x\sqrt{\frac{1}{2}c^TA^{-1}c\,} = A^{-1}c\\
                x = \frac{A^{-1}c}{\sqrt{2c^TA^{-1}c\,}}
            \end{gather*}
        \end{csolution}
        \makeline
        \begin{csolution} % (3.c)
            ~

            Find the gradient:
            \begin{gather*}
                d f(X) = d\left(\sprod{X^{-1}}{I_n} - \sprod{A}{X}\right) = \\
                = \sprod{dX^{-1}}{I_n} - d\sprod{A}{X} = \\
                = \sprod{-X^{-1}\left(dX\right)X^{-1}}{I_n} - \sprod{A}{dX} = \\
                = \sprod{-X^{-T}I_nX^{-T}}{dX} - \sprod{A}{dX} = \\
                = \sprod{- X^{-2T} - A}{dX}
            \end{gather*}

            Using the gradient, find the stationary points:
            \begin{gather*}
                \nabla f(X) = - X^{-2T} - A = 0\\
                X^{-2T} = -A\\
                X^{-2} = -A\\
                X = \left(-A\right)^{-\frac{1}{2}}
            \end{gather*}

            The stationary point exists if and only if $A$ is strictly negative definite.
        \end{csolution}

        \makeline\\[-0.75\baselineskip]\makeline
        \begin{problem} % (4)
            The 2D convolution operation for a given 3D image $X \in \mathbb{R}^{H \times W \times C}$ with some kernel $K \in \mathbb{R}^{h \times w \times C}$ is calculated as follows:

            \[
                Y=X * K, \quad Y_{ij}=\sum_{u, v, w=0}^{h-1, w-1, C-1} K_{u v w} X_{i+u, j+v, w}, 0 \leq i \leq H-h, 0 \leq j \leq W-w
            \]

            Let $f$ be some scalar loss function that depends on $Y$.
            The task is to calculate $\nabla_{K} f$ and $\nabla_{X} f$ for given $\nabla_{Y} f$.
            This calculation should be represented as a convolution of some image with some kernel.
        \end{problem}
        \makeline
        \begin{solution}
            ~

            Let us define a helper pad operator, which given an image or kernel $Z$, reindexes it and adds $n$ zeros on each side along the first axis and $m$ zeros along the second axis:
            \begin{gather*}
                \pad\left(Z,\,m,\,n\right)_{ijk} = \begin{cases}
                    Z_{i-m,\,j-n,\,k}\quad\text{when}\quad\begin{matrix}
                        m \leqslant i \leqslant m + \height\left(Z\right) - 1\\
                        n \leqslant j \leqslant n + \width\left(Z\right) - 1
                    \end{matrix}\\
                    0\,\quad\qquad\qquad\text{otherwise}
                \end{cases}
            \end{gather*}

            And a helper flip operator, which given an image or kernel $Z$, reflects it along both axes:
            \begin{gather*}
                \flip\left(Z\right)_{ijk} = Z_{\height\left(Z\right)-1-i,\,\width\left(Z\right)-1-j,\,k}
            \end{gather*}

            Find the partial derivatives of each output element with respect to each element of the left and the right argument of the convolution:
            \begin{align*}
                \frac{\partial Y_{ij}}{\partial X_{abc}} &= \begin{cases}
                    K_{a-i,\,b-j,\,c}\quad\text{when}\quad\begin{matrix}
                        0 \leqslant a - i \leqslant h - 1\\
                        0 \leqslant b - j \leqslant w - 1
                    \end{matrix}\quad\text{or}\quad\begin{matrix}
                        a - h + 1 \leqslant i \leqslant a\\
                        b - w + 1 \leqslant j \leqslant b
                    \end{matrix}\quad\\
                    0\,\quad\qquad\qquad\text{otherwise}
                \end{cases} \\[4\jot]
                \frac{\partial Y_{ij}}{\partial K_{abc}} &= \begin{cases}
                    X_{a+i,\,b+j,\,c}\quad\text{when}\quad\begin{matrix}
                        0 \leqslant a + i \leqslant H - 1\\
                        0 \leqslant b + j \leqslant W - 1
                    \end{matrix}\\
                    0\,\quad\qquad\qquad\text{otherwise}
                \end{cases}
            \end{align*}

            Then, find the partial derivatives of $f$ with respect to each element of the left argument of the convolution:
            \begin{gather*}
                \frac{\partial f}{\partial X_{abc}} = \sum_{i,j=0}^{\substack{H-h\\W-w}} \frac{\partial f}{\partial Y_{ij}} \frac{\partial Y_{ij}}{\partial X_{abc}} = \sum_{\substack{i=\max\left(0,a-h+1\right)\\j=\max\left(0,b-w+1\right)}}^{\substack{\min\left(a,H-h\right)\\\min\left(b,W-w\right)}} \frac{\partial f}{\partial Y_{ij}} K_{a-i,\,b-j,\,c} = \\
                = \sum_{\substack{i=a-h+1\\j=b-w+1}}^{a,b} \pad\left(\nabla_Y f,\,h-1,\,w-1\right)_{i+h-1,\,j+w-1} K_{a-i,\,b-j,\,c} = \\
                = \left\{\begin{matrix}
                    i=a-h+1+p\\
                    j=b-w+1+q
                \end{matrix}\quad\text{and}\quad\begin{matrix}
                    p=i-a+h-1\\
                    q=j-b+w-1
                \end{matrix}\right\} = \\
                = \sum_{p,q=0}^{h-1,w-1} \pad\left(\nabla_Y f,\,h-1,\,w-1\right)_{a+p,\,b+q} K_{h-1-p,\,w-1-q,\,c} = \\
                = \sum_{p,q=0}^{h-1,w-1} \flip\left(K\right)_{pqc} \pad\left(\nabla_Y f,\,h-1,\,w-1\right)_{a+p,\,b+q}
            \end{gather*}

            Then, find the partial derivatives of $f$ with respect to each element of the right argument of the convolution:
            \begin{gather*}
                \frac{\partial f}{\partial K_{abc}} = \sum_{i,j=0}^{\substack{H-h\\W-w}} \frac{\partial f}{\partial Y_{ij}} \frac{\partial Y_{ij}}{\partial K_{abc}} = \sum_{i,j=0}^{\substack{H-h\\W-w}} \frac{\partial f}{\partial Y_{ij}} X_{a+i,\,b+j,\,c}
            \end{gather*}

            And finally express the above equations in terms of convolutions:
            \begin{gather*}
                \nabla_{X_{:,:,c}} f = \pad\left(\nabla_Y f,\,h-1,\,w-1\right) * \flip\left(K_{:,:,c}\right)\\[2\jot]
                \nabla_{K_{:,:,c}} f = X_{:,:,c} * \nabla_Y f
            \end{gather*}

            Note, that here the convolution operator $*$ means a single-channel 2D convolution rather than the multichannel version.
        \end{solution}

        \makeline\\[-0.75\baselineskip]\makeline
        \begin{problem} % (5)
            Consider a symmetric matrix $X \in \mathbb{R}^{n \times n}$ and its eigenvalue decomposition $X=Q^{T} \Lambda Q$, where $Q$ - orthogonal matrix consisting of eigenvectors, and $\Lambda$ - diagonal matrix with eigenvalues.
            Suppose additionally that all eigenvalues are different.
            Let $f$ be some scalar loss function that depends on eigenvalues and eigenvectors of $X$.
            The task is to calculate $\nabla_{X} f$ for given $\nabla_{\Lambda} f$ (diagonal matrix) and $\nabla_{Q} f$.
        \end{problem}
        \makeline
        \begin{solution}
            ~

            Let $X = X\left(\Lambda, Q\right)$, then
            \begin{gather*}
                d\left(X\right) = d\left(Q^T \Lambda Q\right) = \\
                = d\left(Q^T\right) \Lambda Q + Q^T d\left(\Lambda\right) Q + Q^T \Lambda d\left(Q\right) = \\
                = Q^T Q d\left(Q^T\right) \Lambda Q + Q^T d\left(\Lambda\right) Q + Q^T \Lambda d\left(Q\right) =
            \end{gather*}

            Substitute $d\left(Q\right) = d\left(Q^{-1}\right)^T = \left(- Q^{-1} d\left(Q\right) Q^{-1} \right)^T = - Q d\left(Q\right)^T Q$:
            \begin{gather*}
                = Q^T Q d\left(Q^T\right) \Lambda Q + Q^T d\left(\Lambda\right) Q - Q^T \Lambda Q d\left(Q^T\right) Q = \\
                = Q^T \left(Q d\left(Q^T\right) \Lambda + d\left(\Lambda\right) - \Lambda Q d\left(Q^T\right) \right) Q = \\
                = Q^T \left(d\left(\Lambda\right) + Q d\left(Q^T\right) \Lambda - \Lambda Q d\left(Q^T\right) \right) Q
            \end{gather*}

            Multiply both sides by $Q$ on the left and $Q^T$ on the right:
            \begin{gather*}
                Q d\left(X\right) Q^T = d\left(\Lambda\right) + Q d\left(Q^T\right) \Lambda - \Lambda Q d\left(Q^T\right)
            \end{gather*}

            Notice, that since
            \begin{gather*}
                Q d\left(Q^T\right) = Q \left(d\left(Q\right)\right)^T = \\
                = Q \left(- Q d\left(Q\right)^T Q\right)^T = \\
                = \left(- Q d\left(Q\right)^T Q Q^T\right)^T = \\
                = - \left(Q d\left(Q^T\right)\right)^T
            \end{gather*}
            $Q d\left(Q^T\right)$ is an anti-symmetric matrix and so $Q d\left(Q^T\right) \Lambda$ is also anti-symmetric (since $\Lambda = \Lambda^T$).
            For any anti-symmetric matrix $R$, the matrix $R - R^T$ has only zeroes on the diagonal.
            Conversely, since $\Lambda$ is a diagonal matrix, $d\left(\Lambda\right)$ is also a diagonal matrix.
            \\[\baselineskip]
            Therefore, the equation we just derived can be factorized as follows:
            \begin{gather*}
                Q d\left(X\right) Q^T = \underbrace{d\left(\Lambda\right)}_{\substack{\text{diagonal}\\\text{only}}} + \underbrace{
                    Q d\left(Q^T\right) \Lambda - \Lambda Q d\left(Q^T\right)
                }_{\substack{\text{non-diagonal}\\\text{only}}}
            \end{gather*}

            Furthermore, given an arbitrary "non-diagonal" matrix $Y$ and a diagonal matrix $\Lambda$, such that
            \begin{gather*}
                Y = Z \Lambda - \Lambda Z
            \end{gather*}

            We can solve for $Z$:
            \begin{gather*}
                Y_{ij} = Z_{ij} \Lambda_j - \Lambda_i Z_{ij}\\
                Y_{ij} = \left(\Lambda_j - \Lambda_i\right) Z_{ij}\\
                Z_{ij} = \begin{cases}
                    \frac{Y_{ij}}{\Lambda_j - \Lambda_i}\quad i \neq j\\
                    0\qquad i = j
                \end{cases}
            \end{gather*}

            Giving us:
            \begin{gather*}
                Z = K \circ Y\\
                \text{where } K = \begin{cases}
                    \frac{1}{\Lambda_j - \Lambda_i}\quad i \neq j\\
                    0\qquad i = j
                \end{cases}
            \end{gather*}

            Applying all of the above, we get:
            \begin{gather*}
                Q d\left(Q^T\right) = K \circ \left(Q d\left(X\right) Q^T\right)\\
                d\left(Q^T\right) = Q^T \left(K \circ \left(Q d\left(X\right) Q^T\right)\right)\\
                d\left(Q\right) = \left(K^T \circ \left(Q d\left(X^T\right) Q^T\right)\right) Q\\
                d\left(\Lambda\right) = I \circ \left(Q d\left(X\right) Q^T\right)
            \end{gather*}

            Finally, pluggin the above into the gradient formula:
            \begin{gather*}
                \sprod{\nabla_\Lambda f}{d\Lambda} + \sprod{\nabla_Q f}{dQ} = \\
                = \sprod{\nabla_\Lambda f}{I \circ \left(Q d\left(X\right) Q^T\right)} + \sprod{\nabla_Q f}{\left(K^T \circ \left(Q d\left(X^T\right) Q^T\right)\right) Q} = \\
                = \sprod{\nabla_\Lambda f}{I \circ \left(Q d\left(X\right) Q^T\right)} + \sprod{\nabla_Q f Q^T}{K^T \circ \left(Q d\left(X^T\right) Q^T\right)} = \\
                = \sprod{I \circ \nabla_\Lambda f}{Q d\left(X\right) Q^T} + \sprod{K^T \circ \left(\nabla_Q f Q^T\right)}{Q d\left(X^T\right) Q^T} = \\
                = \sprod{I \circ \nabla_\Lambda f + K^T \circ \left(\nabla_Q f Q^T\right)}{Q d\left(X^T\right) Q^T} = \\
                = \sprod{Q^T \left(I \circ \nabla_\Lambda f + K^T \circ \left(\nabla_Q f Q^T\right)\right) Q}{dX^T} = \\
                = \sprod{Q^T \left(I \circ \nabla_\Lambda f + K \circ \left(\nabla_Q f Q^T\right)^T\right) Q}{dX} = \\
                = \sprod{Q^T \left(I \circ \nabla_\Lambda f + K \circ \left(Q {\nabla_Q f}^T\right)\right) Q}{dX} = \\
                = \sprod{\nabla_X f}{dX}
            \end{gather*}

            Therefore,
            \begin{gather*}
                \nabla_X f = Q^T \left(I \circ \nabla_\Lambda f + K \circ \left(Q {\nabla_Q f}^T\right)\right) Q
            \end{gather*}


            Here is a simple \texttt{Python} program, implementing the above formula and checking the produced gradient against the one produced by \texttt{torch}.
            \begin{lstlisting}[gobble=12,language=Python]
            import torch as tc
            import numpy as np
            from scipy.stats import ortho_group

            def torch_eig_grad(X, L, Q, dL, dQ):
                xt = tc.from_numpy(X).requires_grad_()
                dlt = tc.from_numpy(dL)
                dqt = tc.from_numpy(dQ)

                lt, qt = tc.linalg.eig(xt)
                lt = tc.diag(lt)
                qt = qt.T

                # Torch sometimes provides a different equivalent decomposition, try again
                if not np.allclose(lt.detach().numpy(), L): return None
                if not np.allclose(qt.detach().numpy(), Q): return None

                lt = (lt * dlt).sum()
                qt = (qt * dqt).sum()
                (lt + qt).backward()
                grad = xt.grad.numpy()

                return grad

            def manual_eig_grad(X, L, Q, dL, dQ):
                r = range(X.shape[0])
                l = L[r, r]

                K_inv = l[None, :] - l[:, None]
                K_inv[r, r] = np.inf
                K = 1 / K_inv
                I = np.eye(X.shape[0])

                return Q.T @ (I * dL + K * (Q @ dQ.T)) @ Q

            def make_XQL(size):
                Q = ortho_group.rvs(size)
                L = np.diag(np.sort(np.random.randn(size))[::-1])
                X = Q.T @ L @ Q

                # L and Q values are ambiguous, recompute
                L, Q = np.linalg.eig(X)
                L = np.diag(L)
                Q = Q.T
                assert(np.allclose(X, Q.T @ L @ Q))
                return X, Q, L

            def main():
                dX = None
                while dX is None:
                    X, Q, L = make_XQL(10)
                    dL = np.random.randn(10, 10)
                    dQ = np.random.randn(10, 10)
                    dX = torch_eig_grad(X, L, Q, dL, dQ)

                assert(np.allclose(
                    dX,
                    manual_eig_grad(X, L, Q, dL, dQ),
                ))

            if __name__ == "__main__":
                main()
            \end{lstlisting}
        \end{solution}

        \makeline\\[-0.75\baselineskip]\makeline
        \begin{problem} % (6)
            Consider a symmetric positively defined matrix $X \in \mathbb{S}_{++}^{n}$ and its Cholesky decomposition $X=L L^{T}$, where $L$ - some lower-triangular matrix with positive elements on its diagonal.
            The matrix $L$ can be calculated using the following algorithm:

            % @formatter:off
            \begin{lstlisting}[gobble=12]
            for <@$i$@> from <@$1$@> to <@$n$@>:
                for <@$j$@> from <@$1$@> to <@$i$@>:
                    for <@$k$@> from <@$1$@> to <@$j-1$@>:
                        <@$\displaystyle X_{ij} := X_{ij}-L_{i k} L_{j k}$@>

                    if <@$j=i$@>:
                        <@$\displaystyle L_{ii} := \sqrt{X_{ii}\,}$@>
                    else:
                        <@$\displaystyle L_{ij} := \frac{X_{ij}}{L_{jj}}$@>
            \end{lstlisting}
            % @formatter:on

            Let $f$ be some scalar loss function that depends on $L$.
            The task is to find an algorithm for calculating $\nabla_{X} f$ for given $\nabla_{L} f$ (lower-triangular matrix).
        \end{problem}
        \makeline
        \begin{solution}
            ~

            Before we start deriving the gradient, let us simplify the above algorithm.
            We apply the following transformations to the algorithm:
            \begin{enumerate}[label={\arabic*.}]
                \item Replace $L_{jj}$ with $\sqrt{X_{jj}\,}$. (in the denominator of the \texttt{else} branch)

                \item Replace $\sqrt{X_{jj}\,}$ with $\frac{X_{jj}}{\sqrt{X_{jj}\,}}$. (in the first \texttt{if} branch)

                \item After this, both branches of the \texttt{if} statement are the same and can be replaced with a single unconditional assignment.

                \item Instead of performing inplace operations on $X_{ij}$, compute the sum in a single step and store the intermediate result into a separate variable $O_{ij}$.
            \end{enumerate}

            Giving us the following algorithm:
            % @formatter:off
            \begin{lstlisting}[gobble=12]
            for <@$i$@> from <@$1$@> to <@$n$@>:
                for <@$j$@> from <@$1$@> to <@$i$@>:
                    <@$\displaystyle O_{ij} = X_{ij} - \sum_{k=1}^{j-1} L_{i k} L_{j k}$@> # (1)
                    <@$\displaystyle L_{ij} = \frac{O_{ij}}{\sqrt{O_{jj}\,}}$@> # (2)
            \end{lstlisting}
            % @formatter:on

            Unlike in the original algorithm, in our modified algorithm every variable is assigned \textbf{exactly once} and so a standard backwards propagation algorithm can be used to derive and calculate $\nabla_X f$.\\[0.5\baselineskip]
            We derive the partial derivatives for the first line:
            \[
                \frac{\partial O_{ij}}{\partial X_{ij}} = 1\qquad\qquad\qquad
                \frac{\partial O_{ij}}{\partial L_{i k}} = -L_{j k}\qquad\qquad\qquad
                \frac{\partial O_{ij}}{\partial L_{j k}} = -L_{i k}
            \]
            And for the second line:
            \[
                \frac{\partial L_{ij}}{\partial O_{ij}} = \frac{1}{\sqrt{O_{jj}\,}} = \frac{1}{L_{jj}}\qquad\qquad\qquad
                \frac{\partial L_{ij}}{\partial O_{jj}} = -\frac{1}{2 O_{jj}} \frac{O_{ij}}{\sqrt{O_{jj}\,}} = -\frac{L_{ij}}{2 L_{jj}^2}
            \]

            Let
            \begin{align*}
                \widehat{L} &= \nabla_L f\qquad\qquad\quad\ \ \text{(backprop input, the known gradient)}\\
                \widehat{O} &= \nabla_O f = \nabla_X f\qquad\text{(backprop output, initialized to zero)}
            \end{align*}
            Then the backward gradient propagation algorithm can be written as:
            % @formatter:off
            \begin{lstlisting}[gobble=12]
            for <@$i$@> from <@$n$@> to <@$1$@>:
                for <@$j$@> from <@$i$@> to <@$1$@>:
                    # backprop of line 2
                    <@$\displaystyle \widehat{O}_{ij} := \widehat{O}_{ij} + \frac{\widehat{L}_{ij}}{L_{jj}}$@>
                    <@$\displaystyle \widehat{O}_{jj} := \widehat{O}_{jj} - \frac{\widehat{L}_{ij} L_{ij}}{2L_{jj}^{2}}$@>

                    # backprop of line 1
                    for <@$k$@> from <@$j-1$@> to <@$1$@>:
                        <@$\displaystyle \widehat{L}_{ik} := \widehat{L}_{ik} - \widehat{O}_{ij} L_{jk}$@>
                        <@$\displaystyle \widehat{L}_{jk} := \widehat{L}_{jk} - \widehat{O}_{ij} L_{ik}$@>
            \end{lstlisting}
            % @formatter:on

            Here is a simple \texttt{Python} program, implementing the above algorithm and checking the produced gradient against the one produced by \texttt{torch}.
            \begin{lstlisting}[gobble=12,language=Python]
            import torch as tc
            import numpy as np

            def torch_cholesky_grad(X, L, G):
                del L
                xt = tc.from_numpy(X).requires_grad_()
                gt = tc.from_numpy(G)
                lt = gt * tc.linalg.cholesky(xt)
                lt.sum().backward()
                grad = xt.grad.numpy()

                # There is an ambiguity with how the algorithm should be
                # represented, considering that X is a symmetric matrix.
                # torch disagrees with the provided algorithm.
                # Convert the gradient to its lower-triangular form:
                low = np.tri(grad.shape[0], k=-1) * grad
                grad += low
                grad -= low.T
                return grad

            def manual_cholesky_grad(X, L, G):
                n, _ = X.shape
                X = X.copy()
                L = L.copy()
                L_hat = G.copy()
                X_hat = np.zeros_like(X)

                for i in range(n)[::-1]:
                    for j in range(1+i)[::-1]:
                        X_hat[i,j] += L_hat[i,j]          / L[j,j]
                        X_hat[j,j] -= L_hat[i,j] * L[i,j] / (2 * L[j,j]**2)

                        for k in range(j)[::-1]:
                            L_hat[i,k] -= X_hat[i,j] * L[j,k]
                            L_hat[j,k] -= X_hat[i,j] * L[i,k]

                return X_hat

            def main():
                X = np.random.randn(10, 10)
                G = np.random.randn(10, 10)
                X = X @ X.T
                L = np.linalg.cholesky(X)
                assert(np.allclose(
                    torch_cholesky_grad(X,L,G),
                    manual_cholesky_grad(X,L,G),
                ))

            if __name__ == "__main__":
                main()
            \end{lstlisting}
        \end{solution}
    \end{preview}
\end{document}
