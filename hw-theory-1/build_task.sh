#!/bin/sh
set -euo pipefail
cd "$(dirname "$0")"

latexmk -pdf -pdflatex="texfot pdflatex -interaction=nonstopmode -file-line-error -halt-on-error" -cd -outdir=build task.tex
